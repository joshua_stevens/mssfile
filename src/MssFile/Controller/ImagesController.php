<?php

namespace MssFile\Controller;

use Zend\Mvc\Controller\ActionController;

class ImagesController extends ActionController
{
    public function indexAction()
    {
        $security = $this->getLocator()->get('mssuser_security_service');
        if (!$security->isRouteAllowed($this->getEvent())) {
            return $this->redirect()->toRoute('mssuser/login');
        }

        $user = $this->getLocator()->get('mssuser_user_service')->getAuthService()->getIdentity();
        $em   = $this->getLocator()->get('doctrine_em');
        $data = $em->getRepository('MssFile\Entity\File')
                   ->findImagesGridArrayByCompany($user->getCompany()->getId());

        $svc  = $this->getLocator()->get('spiffydatatables_data_service');
        $data = $svc->format($data, array(
            'edit' => array(
                'type'    => 'link',
                'insert'  => 'append',
                'options' => array(
                    'label' => 'Edit',
                    'link'  => '/files/images/edit/%id%',
                )
            )
        ));

        return array('data' => $data);
    }
    
    public function addAction()
    {
        return $this->process();
    }

    public function editAction()
    {
        $match  = $this->getEvent()->getRouteMatch();
        $data   = $this->getEntityManager()->find(
            'MssFile\Entity\File', 
            $match->getParam('id')
        );
        
        return $this->process($data);
    }
    
    public function getEntityManager()
    {
        return $this->getLocator()->get('doctrine_em');
    }

    protected function process($data = null) 
    {
        $security = $this->getLocator()->get('mssuser_security_service');
        if (!$security->isRouteAllowed($this->getEvent())) {
            return $this->redirect()->toRoute('mssuser/login');
        }

        $request = $this->getRequest();
        $manager = $this->getLocator()->get('spiffyform_builder_orm', array(
            'definition' => 'mssfile_image_definition',
            'data'       => $data,
            'em'         => $this->getEntityManager()
        ));

        if ($request->isPost() && $manager->isValid($request->post())) {
            if ($manager->getForm()->getValue('cancel')) {
                $message = ':notice:Image update cancelled';
            } else {
                $company = $this->getLocator()->get('mssuser_user_service')->getAuthService()->getIdentity()->getCompany();
                $manager->getData()->setCompany($this->getEntityManager()->getReference('MssCompany\Entity\Company', $company->getId()));
                
                if ($manager->getForm()->file->isUploaded()) {
                    preg_match('/\.(jpeg|jpg|png|gif)$/i', $manager->getForm()->file->getValue(), $matches);
                    $filename = strtolower(sprintf('%s.%s', uniqid(), $matches[1]));
                    $dest     = sprintf('/home/vetlogic/files/company/%s/upload', $company->getId());
                    
                    if (!file_exists($dest)) {
                        mkdir($dest, 0755, true);
                    }
                    
                    chmod($manager->getForm()->file->getFilename(), 0644);
                    rename($manager->getForm()->file->getFilename(), $dest . '/' . $filename);
                    
                    $manager->getData()->setFilename($filename);
                }
                
                $this->getEntityManager()->persist($manager->getData());
                $this->getEntityManager()->flush();

                $message = $data ? ':success:Image updated' : ':success:Image added';
            }

            $this->plugin('flashMessenger')
                 ->setNamespace('spiffy_notify')
                 ->addMessage($message);

            return $this->redirect()->toRoute('mssfile/images');
        }
        
        return array('manager' => $manager);
    }
}