<?php

namespace MssFile\Repository;

use Doctrine\ORM\EntityRepository;

class File extends EntityRepository
{
    protected $extensions = array(
        'image' => array(
            'bmp',
            'gif',
            'jpg',
            'jpeg',
            'png',
        ),
    );
    
    public function findImagesGridArrayByCompany($companyId)
    {
        $query = $this->_em->createQuery("
            SELECT 
                f.id,
                f.name,
                f.description 
            FROM MssFile\Entity\File f
            WHERE f.company = ?1
        ");
        return $query->execute(array(1 => $companyId));
    }
    
    public function findImagesArrayByCompany($companyId)
    {
        return $this->_em->createQuery("
            SELECT f
            FROM MssFile\Entity\File AS f
            WHERE f.company = ?1
            AND ({$this->getExtQuery('image')})
        ")->execute(array(1 => $companyId));
    }
    
    private function getExtQuery($extName)
    {
        if (!isset($this->extensions[$extName])) {
            throw new InvalidArgumentException(sprintf(
                'no extension definitions for %s',
                $extName
            ));
        }

        $extra = array();
        foreach($this->extensions[$extName] as $ext) {
            $extra[] = "f.filename LIKE '%.{$ext}'";
        }
        return implode(' OR ', $extra);
    }
}