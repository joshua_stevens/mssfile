<?php

namespace MssFile\Form;

use SpiffyAnnotation\Form,
    SpiffyForm\Form\Builder,
    SpiffyForm\Form\Definition;

class Image extends Definition
{
    public function build(Builder $m)
    {
        $m->add('name')
          ->add('description')
          ->add('file', 'file', array(
            'required' => false,
            'maxFileSize' => 5120,
            'validators' => array(
                array('extension', false, 'jpeg,jpg,png,gif'),
                array('size', false, '5120'),
            )
          ))
          ->add('submit', 'submit', array(
              'label' => 'Save Changes',
          ))
          ->add('cancel', 'submit', array(
              'label' => 'Cancel Changes'
            ));
    }

    public function getName()
    {
        return 'update';
    }

    public function getOptions()
    {
        return array(
            'attribs' => array('enctype' => 'multipart/form-data'),
            'data_class' => 'MssFile\Entity\File'
        );
    }
}
