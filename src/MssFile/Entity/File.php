<?php

namespace MssFile\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MssFile\Repository\File")
 * @ORM\Table(name="file")
 */
class File
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $filename;
    
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    
    /**
     * @ORM\Column(type="string")
     */
    private $description;
    
    /**
     * @ORM\ManyToOne(targetEntity="MssCompany\Entity\Company")
     * @ORM\JoinColumn(name="companyId", nullable=false)
     */
    private $company;

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Set id
     *
     * @param int $id
     * @return MssMessage\Entity\File
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

	/**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

	/**
     * Set filename
     *
     * @param string $filename
     * @return MssMessage\Entity\File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

	/**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

	/**
     * Set name
     *
     * @param string $name
     * @return MssMessage\Entity\File
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

	/**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

	/**
     * Set description
     *
     * @param string $description
     * @return MssMessage\Entity\File
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

	/**
     * Get company
     *
     * @return MssCompany\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

	/**
     * Set company
     *
     * @param MssCompany\Entity\Company $company
     * @return MssMessage\Entity\File
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }
    
    /**
     * Gets the public encoded url for the file.
     * 
     * @return string
     */
    public function getPublicUrl()
    {
        $filename  = '/company/' . $this->getCompany()->getId() . '/upload/' . $this->filename;
        $extension = substr($this->filename, strrpos($filename, '.'));
        $filename  = strtr(base64_encode(addslashes(gzcompress($filename, 9))), '+/=', '-_,') . $extension;
        
        return 'http://static.petwisewebsites.com/' . $filename;
    }
}