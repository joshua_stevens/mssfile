<?php
return array(
    'di' => array(
        'instance' => array(
            'alias' => array(
                // controllers
                'mssfile_images' => 'MssFile\Controller\ImagesController',
                
                // forms
                'mssfile_image_definition' => 'MssFile\Form\Image',
            ),
            'orm_driver_chain' => array(
                'parameters' => array(
                    'drivers' => array(
                        'mssfile_annotationdriver' => array(
                            'class'           => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                            'namespace'       => 'MssFile\Entity',
                            'paths'           => array(__DIR__ . '/../src/MssFile/Entity'),
                        ),
                    )
                )
            ),
            'Zend\View\PhpRenderer' => array(
                'parameters' => array(
                    'options' => array(
                        'script_paths' => array(
                            'mssfile' => __DIR__ . '/../views',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'routes' => array(
        'mssfile' => array(
            'type' => 'Literal',
            'options' => array(
                'route' => '/files',
                'defaults' => array(
                    'controller' => 'mssfile'
                )
            ),
            'may_terminate' => true,
            'child_routes' => array(
                'images' => array(
                    'type' => 'Literal',
                    'options' => array(
                        'route' => '/images',
                        'defaults' => array(
                            'controller' => 'mssfile_images',
                        )
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                        'add' => array(
                            'type' => 'Literal',
                            'options' => array(
                                'route' => '/add',
                                'defaults' => array(
                                    'controller' => 'mssfile_images',
                                    'action'     => 'add'
                                )
                            ),
                        ),
                        'edit' => array(
                            'type' => 'Regex',
                            'options' => array(
                                'regex' => '/edit/(?<id>\d+)',
                                'defaults' => array(
                                    'controller' => 'mssfile_images',
                                    'action' => 'edit',
                                ),
                                'spec' => '/edit/%id%'
                            )
                        )
                    )
                )
            )
        )
    )
);